package com.sda;

public class Main {
    public static void main(String[] args) {
        int [][] matrix = {
                {1, 2, 3, 10, 25, 25},
                {4, 5, 6, 10, 11, 25},
                {1, 1, 3, 10, 10, 10},
                {1, 1, 3, 10,  9, 10},
                {1, 1, 3, 10, 25, 25},
                {2, 2, 2,  4,  4,  2},
                {12, 8, 11, 11, 9, 9},
        };
        Histogram.show(matrix);
    }
}
