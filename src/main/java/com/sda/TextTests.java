package com.sda;

public class TextTests {
    public static boolean isPalindrome(String input) {
        String data = input.toLowerCase();
        int len = data.length();
        for (int i = 0; i < len/2; i++) {
            if (data.charAt(i) != data.charAt(len - 1 -i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAnagram(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        int [] counter = new int['z' - 'a'];
        for (int i = 0; i < str1.length(); i++) {
            counter[str1.charAt(i) - 'a']++;
        }
        for (int i = 0; i < str2.length(); i++) {
            counter[str2.charAt(i) - 'a']--;
        }
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
