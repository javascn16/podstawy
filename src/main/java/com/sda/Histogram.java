package com.sda;

public class Histogram {
    //maksymalna dopuszczalna wartość w tablicy danych wejściowych
    private static final int MAX = 25;
    //szerokość kolumny
    private static final int charWide = 2;

    //Oblicza histogram i rysuje go na ekranie
    public static void show(int[][] matrix) {
        int [] histogram = new int[MAX+1];

        //obliczamy histogram
        //wartość elementu w tablicy 'matrix' jest indeksem w tablicy 'histogram'
        //podm tym indeksem przechowujemy licznik dla określonej wartości:
        // indeks 0 - licznik dla 0
        // indeks 1 - licznik dla 1
        //itd.
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                histogram[matrix[i][j]]++;
            }
        }

        //znajdujemy maksymalną wawrtość w histogramie; jest ona jednocześnie
        //wysokością najwyższego słupka
        int max = histogram[0];
        for (int i = 0; i < histogram.length; i++) {
            if (histogram[i] > max) {
                max = histogram[i];
            }
        }

        //rysujemy histogram
        System.out.println();
        for (int i = 0; i < max; i++) {
            for (int j = 0; j < histogram.length; j++) {
                int wide = charWide;
                while (wide > 0) {
                    //w zależności od wysokości słupka zaczynamy rysować
                    if (histogram[j] >= (max - i)) {
                    //if (histogram[j] > i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                    --wide;
                }
                System.out.print(" ");
            }
            System.out.println();
        }

        //rysujemy linię odcięcia
        int counter = (charWide+1) * (MAX + 1);
        while (counter > 0) {
            System.out.print("=");
            --counter;
        }

        //rysujemy opisy słupków - oznaczamy dla jakiej wartości jest dany słupek
        System.out.println();
        for (int i = 0; i < histogram.length; i++) {
            if (i < 10) {
                System.out.print(i + "  ");
            } else {
                System.out.print(i + " ");
            }
        }
    }
}
