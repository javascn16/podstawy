package com.sda.checkers;

public class Player {
    private PlayerType type;
    int checkerCounter = 12;

    public enum PlayerType {
        PLAYER_1,
        PLAYER_2,
        NONE
    }

    public Player(PlayerType type) {
        this.type = type;
    }

    public PlayerType getType() {
        return type;
    }

    public static String getPlayerSymbol(PlayerType type) {
        if (PlayerType.PLAYER_1 == type) {
            return "B";
        } else if (PlayerType.PLAYER_2 == type) {
            return "C";
        } else {
            return " ";
        }
    }

    public void lostChecker() {
        checkerCounter--;
    }

    public int getCheckersCount() {
        return checkerCounter;
    }
}
