package com.sda.checkers;

public class Checkerboard {
    private static final int SIZE = 8;

    private Field[][] board = new Field[SIZE][SIZE];

    public Checkerboard() {
        initialise();
    }

    public Field getField(int row, int column) {
        //TODO: check bounds
        return board[row][column];
    }

    public void draw() {
        for (int row = 0; row < SIZE; row++) {
            for (int column = 0; column < SIZE; column++) {
                StringBuilder builder = new StringBuilder();
                Field field = board[row][column];
                if (field.getFieldColor() == Field.Color.WHITE) {
                    builder.append(ANSIColors.WHITE_BACKGROUND);
                    builder.append(ANSIColors.BLACK);
                    Player.PlayerType type = field.getOwner();
                    builder.append(" " + Player.getPlayerSymbol(type) + " ");
                } else {
                    builder.append(ANSIColors.BLACK_BACKGROUND);
                    builder.append(ANSIColors.WHITE);
                    Player.PlayerType type = field.getOwner();
                    builder.append(" " + Player.getPlayerSymbol(type) + " ");
                }
                builder.append(ANSIColors.RESET);
                System.out.print(builder.toString());
            }
            System.out.print(System.lineSeparator());
        }
    }

    private void initialise() {
        boolean isFirstWhite = true;
        for (int row = 0; row < SIZE; row++) {
            for (int column = 0; column < SIZE; column++) {
                Field.Color color;
                if (isFirstWhite) {
                    color = (column+1) % 2 == 0 ? Field.Color.BLACK : Field.Color.WHITE;
                } else {
                    color = (column+1) % 2 != 0 ? Field.Color.BLACK : Field.Color.WHITE;
                }
                Player.PlayerType playerType;
                if (row <= 2 && color == Field.Color.BLACK) {
                    playerType = Player.PlayerType.PLAYER_2;
                } else if (row >= (SIZE - 3) && color == Field.Color.BLACK) {
                    playerType = Player.PlayerType.PLAYER_1;
                } else {
                    playerType = Player.PlayerType.NONE;
                }
                board[row][column] = new Field(color, playerType);
            }
            isFirstWhite = !isFirstWhite;
        }
    }
}
