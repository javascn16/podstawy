package com.sda.checkers;

public class Coordinates {
    public int row;
    public char column;

    public Coordinates(int row, char column) {
        this.row = row;
        this.column = column;
    }

    int getRowIndex() {
        //row indices are inverted
        return 8 - row;
    }

    int getColumnsIndex() {
        return column - 'a';
    }
}
