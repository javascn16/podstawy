package com.sda.checkers;

/**
 * Class represents move coordinates.
 * Coordinates are expressed as row:column, eg.: 3:a
 */
public class Move {
    private final int MOVE_INPUT_LENGTH = 3;

    private Coordinates from;
    private Coordinates to;

    public Move(String from, String to) {
        parse(from.toLowerCase(), to.toLowerCase());
    }

    public boolean isValid() {
        if (from == null || to == null) {
            return false;
        }
        return true;
    }

    public boolean isQuit() {
        return false;
    }

    public Coordinates getFrom() {
        return from;
    }

    public Coordinates getTo() {
        return to;
    }

    private void parse(String from, String to) {
        this.from = parseMove(from);
        if (this.from != null) {
            this.to = parseMove(to);
        }
    }

    private Coordinates parseMove(String move) {
        if (move.length() != MOVE_INPUT_LENGTH) {
            return null;
        }
        int colonIdx = move.indexOf(':');
        if (colonIdx < 0) {
            return null;
        }
        String rowStr = move.substring(0, colonIdx);
        String colStr = move.substring(colonIdx + 1);
        int row;
        try {
            row = Integer.parseInt(rowStr);
        } catch (NumberFormatException e) {
            return null;
        }
        char column = colStr.charAt(0);
        if (column < 'a' || column > 'h') {
            return null;
        }
        return new Coordinates(row, column);
    }
}
