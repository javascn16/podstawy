package com.sda.checkers;

public class Field {
    private Color fieldColor;
    private Player.PlayerType owner;

    public enum Color {
        WHITE,
        BLACK
    }

    public Field(Color fieldColor, Player.PlayerType playerType) {
        this.fieldColor = fieldColor;
        this.owner = playerType;
    }

    public Color getFieldColor() {
        return fieldColor;
    }
    public Player.PlayerType getOwner() {
        return owner;
    }
    public void setOwner(Player.PlayerType newOwner) {
        owner = newOwner;
    }
}
