package com.sda.checkers;

import java.util.Scanner;

public class CheckersGame {
    private static final String EXIT_SIGN = "q";
    private Checkerboard board;
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private boolean isGameOver = false;
    Scanner scanner;

    public CheckersGame() {
        board = new Checkerboard();
        player1 = new Player(Player.PlayerType.PLAYER_1);
        player2 = new Player(Player.PlayerType.PLAYER_2);
        currentPlayer = player1;
        scanner = new Scanner(System.in);
    }

    public void start() {
        // show game info
        while (!isGameOver) {
            //1. drawing
            draw();
            //2. ask for move (just before move check if over;
            // user can type exit or resign command)
            Move move = readMove();
            if (!move.isValid()) {
                System.out.println("Invalid move!");
                continue;
            }
            if (move.isQuit()) {
                isGameOver = true;
                continue;
            }
            //3. execute move (test for valid move)
            executeMove(move);
            //4. change player
            changePlayer();
        }
    }

    private void draw() {
        board.draw();
    }

    private Move readMove() {
        System.out.println("Make your move. Select start field:");
        String from = scanner.nextLine();
        System.out.println("Select target field:");
        String to = scanner.nextLine();
        if (from.toLowerCase().equals(EXIT_SIGN) || to.toLowerCase().equals(EXIT_SIGN)) {
            isGameOver = true;
        }
        return new Move(from, to);
    }

    private void executeMove(Move move) {
        Field sourceField = board.getField(move.getFrom().getRowIndex(), move.getFrom().getColumnsIndex());
        if (sourceField.getOwner() == Player.PlayerType.NONE) {
            System.out.println("Can't move from empty field!");
            return;
        }
        if (sourceField.getOwner() != currentPlayer.getType()) {
            System.out.println("You can move only your own checkers!");
            return;
        }
        Field targetField = board.getField(move.getTo().getRowIndex(), move.getTo().getColumnsIndex());
        if (targetField.getOwner() != Player.PlayerType.NONE) {
            System.out.println("This field is already occupied!");
            return;
        }
        int deltaWidth = Math.abs(move.getFrom().getColumnsIndex() - move.getTo().getColumnsIndex());
        int deltaHeight = Math.abs(move.getFrom().getRowIndex() - move.getTo().getRowIndex());
        if (deltaHeight != deltaWidth) {
            System.out.println("Incorrect move!");
            return;
        }
        if (deltaHeight == 1) {
            //TODO: check if legal for white and for black
            if (currentPlayer.getType() == Player.PlayerType.PLAYER_1) {
                if (move.getFrom().getRowIndex() > move.getTo().getRowIndex()) {
                    targetField.setOwner(currentPlayer.getType());
                    sourceField.setOwner(Player.PlayerType.NONE);
                } else {
                    System.out.println("Can't move backward!");
                    return;
                }
            }
        } else if (deltaHeight == 2) {
            int middleRow;
            if (move.getFrom().getRowIndex() > move.getTo().getRowIndex()) {
                middleRow = move.getFrom().getRowIndex() - move.getTo().getRowIndex();
            } else {
                middleRow = move.getTo().getRowIndex() - move.getFrom().getRowIndex();
            }
            int middleColumn;
            if (move.getFrom().getColumnsIndex() > move.getTo().getColumnsIndex()) {
                middleColumn = move.getFrom().getColumnsIndex() - move.getTo().getColumnsIndex();
            } else {
                middleColumn = move.getTo().getColumnsIndex() - move.getFrom().getColumnsIndex();
            }
            Field middle = board.getField(middleRow, middleColumn);
            Player.PlayerType middleOwner = middle.getOwner();
            if (middleOwner == currentPlayer.getType() || middleOwner == Player.PlayerType.NONE) {
                System.out.println("can't make that move!");
                return;
            } else {
                middle.setOwner(Player.PlayerType.NONE);
                if (currentPlayer == player1) {
                    player2.lostChecker();
                } else {
                    player1.lostChecker();
                }
            }
        }
    }

    private void changePlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }
}
