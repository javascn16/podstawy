package com.sda;

public class MaxCounter {
    public static void count(int tab[], int min, int max) {
        if (tab == null) {
            return;
        }
        if (min < 0 || max < 0) {
            System.out.println("Negative values not supported!");
            return;
        }
        if (min > max) {
            System.out.println("Min is greater than max!");
            return;
        }
        int [] counters = new int[max+1];
        for (int i = 0; i < tab.length; i++) {
            counters[tab[i]]++;
        }

        int maxCounted = 0;
        for (int i = 0; i < counters.length; i++) {
            if (counters[i] > counters[maxCounted]) {
                maxCounted = i;
            }
        }
        System.out.println("Max counted value is: " + maxCounted);
    }
}
