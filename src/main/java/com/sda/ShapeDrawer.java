package com.sda;

import java.util.Scanner;

public class ShapeDrawer {
    public static void drawSquare(boolean fill) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter edge length: ");
        int size = scanner.nextInt();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0 || i == size-1 || j == 0 || j == size-1) {
                    System.out.print("*");
                } else {
                    if (fill) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void drawRightTriangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter triangle height: ");
        int h = scanner.nextInt();
        System.out.println("Enter triangle base: ");
        int b = scanner.nextInt();
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < b; j++) {
                //TODO: zmodyfikuj tak aby działało dobrze dla h != b
                if (j <= i) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
