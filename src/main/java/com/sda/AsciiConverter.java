package com.sda;

import java.util.Scanner;

public class AsciiConverter {
    public static void toAsciiCode() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter character: ");
        String line = scanner.nextLine();
        if (line.length() > 0) {
            char ch = line.charAt(0);
            System.out.println("Code for character " + ch
                    + " is: " + (int)ch);
        }
    }

    public static void codeToAscii() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter code: ");
        int code = scanner.nextInt();
        System.out.println("Character for ascii code: " + code +
                " is: " + (char)code);
    }
}
