package com.sda;

public class Fibonacci {
    public static void printN(int n) {
        if (n == 0) {
            return;
        }
        int a1 = 0;
        int a2 = 1;
        for (int i = 0; i < n; i++) {
            int tmp = a2 + a1;
            if (tmp < a2) {
                System.out.println("No more space in int!");
                return;
            }
            System.out.print(tmp + " ");
            if (i < 2) {
                if (i == 1) {
                    a1 = 1;
                }
                continue;
            }
            a2 = a1;
            a1 = tmp;
        }
    }
}
