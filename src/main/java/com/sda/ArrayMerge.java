package com.sda;

public class ArrayMerge {
    public static int[] simple(int[] tab1, int[] tab2) {
        if (tab1 == null) {
            return tab2;
        }
        if (tab2 == null) {
            return tab1;
        }
        int[] result = new int[tab1.length + tab2.length];
        for (int i = 0; i < tab1.length; i++) {
            result[i] = tab1[i];
        }
        for (int i = 0; i < tab2.length; i++) {
            result[i + tab1.length] = tab2[i];
        }
        return result;
    }

    public static int[] keepOrderIfPresorted(int[] tab1, int[] tab2) {
        if (tab1 == null) {
            return tab2;
        }
        if (tab2 == null) {
            return tab1;
        }
        int[] result = new int[tab1.length + tab2.length];
        int t1Idx = 0;
        int t2Idx = 0;
        for (int i = 0; i < result.length; i++) {
            if (t1Idx < tab1.length && t2Idx < tab2.length) {
                if (tab1[t1Idx] < tab2[t2Idx]) {
                    result[i] = tab1[t1Idx];
                    t1Idx++;
                } else {
                    result[i] = tab2[t2Idx];
                    t2Idx++;
                }
            } else if (t1Idx >= tab1.length) {
                result[i] = tab2[t2Idx];
                t2Idx++;
            } else {
                result[i] = tab1[t1Idx];
                t1Idx++;
            }
        }
        return result;
    }
}
