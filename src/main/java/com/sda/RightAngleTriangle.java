package com.sda;

public class RightAngleTriangle {

    private static boolean fits(int[] edges, int longestIdx) {
        int hypotenuse = (int) Math.pow(edges[longestIdx], 2);
        int legs = 0;
        for (int i = 0; i < edges.length; i++) {
            if (i != longestIdx) {
                legs += (int) Math.pow(edges[i], 2);
            }
        }
        if (hypotenuse == legs) {
            return true;
        }
        return false;
    }

    public static boolean edgesFit(int a, int b, int c) {
        int [] values = new int[3];
        values[0] = a; values[1] = b; values[2] = c;
        for (int i = 0; i < values.length; i++) {
            if (fits(values, i)) {
                return true;
            }
        }
        return false;
    }
}
