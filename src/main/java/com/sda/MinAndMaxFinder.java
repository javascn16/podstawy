package com.sda;

public class MinAndMaxFinder {
    public static void find(int [] tab) {
        if (tab == null || tab.length == 0) {
            return;
        }
        int min = tab[0];
        int max = tab[0];
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < min) {
                min = tab[i];
            }
            if (tab[i] > max) {
                max = tab[i];
            }
        }
        System.out.println("Min is: " + min + " and max is: " + max);
    }
}
