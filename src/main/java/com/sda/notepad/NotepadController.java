package com.sda.notepad;

import java.util.Scanner;

public class NotepadController {
    private Notepad notepad;
    Scanner scanner;

    public NotepadController(Notepad notepad) {
        this.notepad = notepad;
        scanner = new Scanner(System.in);
    }


    public void run() {
        boolean isFinished = false;
        printUsage();
        while (!isFinished) {
            System.out.println("Choose operation:");
            int operation = Integer.parseInt(scanner.nextLine());
            switch (operation) {
                case 1:
                    enterNote();
                    break;
                case 2:
                    showNote();
                    break;
                case 3:
                    deleteNote();
                    break;
                case 4:
                    editNote();
                    break;
                case 5:
                    listNotes();
                    break;
                case 6:
                    deleteAllNotes();
                    break;
                case 7:
                    printUsage();
                    break;
                case 8:
                    saveToFile();
                case 9:
                    isFinished = true;
                    break;
                default:
                    continue;
            }
        }
    }

    private void printUsage() {
        System.out.println("Welcome to your notepad! Here is what you can do:");
        System.out.println("1. Add note.");
        System.out.println("2. Show note.");
        System.out.println("3. Delete note.");
        System.out.println("4. Edit note.");
        System.out.println("5. List notes.");
        System.out.println("6. Delete all notes.");
        System.out.println("7. Show this help.");
        System.out.println("8. Save to file.");
        System.out.println("9. Exit.");
    }

    private void saveToFile() {
        System.out.println("Enter file name:");
        String filename = scanner.nextLine();
        if (filename.isEmpty() || filename.isBlank()) {
            System.out.println("Incorrect file name!");
            return;
        }
        boolean success = notepad.writeToFile(filename);
        if (success == false){
            System.out.println("Failed to write notes to file!");
        }
    }

    private void deleteAllNotes() {
        System.out.println("Are you sure you want to delete all notes?");
        System.out.print("YES/NO");
        String decision = scanner.nextLine();
        if (decision.toLowerCase().equals("yes") || decision.toLowerCase().equals("y")) {
            notepad.clearNotes();
        }
    }

    private void listNotes() {
        System.out.println("Your notes: ");
        notepad.list();
    }
    private void editNote() {
        System.out.println("Which note would you like to edit? Enter its number.");
        int position = Integer.parseInt(scanner.next());
        String newNote = scanner.nextLine();
        notepad.setNote(newNote, position);
    }
    private void deleteNote() {
        System.out.println("Which note would you like to remove? Enter its number.");
        int position = Integer.parseInt(scanner.nextLine());
        notepad.delete(position);
    }
    private void showNote() {
        System.out.println("Which note would you like to see? Enter its number.");
        int position = Integer.parseInt(scanner.nextLine());
        notepad.printNote(position);
    }
    private void enterNote() {
        System.out.println("Enter note: ");
        String note = scanner.nextLine();
        notepad.addNote(note);
    }
}
