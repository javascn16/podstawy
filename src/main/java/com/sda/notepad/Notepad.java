package com.sda.notepad;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class Notepad {
    private LinkedList<String> notesList = new LinkedList<>();

    public void addNote(String note) {
        if (null == note) {
            System.out.println("Ooops, null!");
            return;
        }
        notesList.add(note);
    }

    public String getNote(int index) {
        if (!isValidIndex(index)) {
            return null;
        }
        return notesList.get(index - 1);
    }

    public void setNote(String note, int index) {
        if (!isValidIndex(index)) {
            return;
        }
        notesList.set((index - 1), note);
    }

    public void printNote(int index) {
        if (!isValidIndex(index)) {
            return;
        }
        System.out.println("Note " + index + ":");
        System.out.println(notesList.get(index-1));
    }

    public void list() {
        int counter = 1;
        for (String note: notesList) {
            int headerLength = note.length() >= 10 ? 10 : note.length();
            String header = note.substring(0, headerLength);
            System.out.println(counter +". " + header + "...");
            counter++;
        }
    }

    public void delete(int index) {
        if(!isValidIndex(index)) {
            return;
        }
        notesList.remove(index - 1);
    }

    public void clearNotes() {
        notesList.clear();
    }

    public int getNotesCount() {
        return notesList.size();
    }

    public boolean writeToFile(String path) {
        try {
            FileWriter fw = new FileWriter(path);
            int counter = 1;
            for (String note : notesList) {
                fw.write(counter + ". " + note + System.lineSeparator());
                counter++;
            }
            fw.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    private boolean isValidIndex(int index) {
        if ((index - 1) >= notesList.size() || (index - 1) < 0) {
            System.out.println("Sorry! There is no note at position: " + index);
            System.out.println("Please try again.");
            return false;
        }
        return true;
    }
}
