package com.sda;

public class ArrayInverter {
    public static void invert(int [] tab) {
        if (tab == null || tab.length < 2) {
            return;
        }
        for (int i = 0; i < tab.length/2; i++) {
            int tmp = tab[i];
            tab[i] = tab[tab.length - 1 - i];
            tab[tab.length - 1 - i] = tmp;
        }
    }
}
