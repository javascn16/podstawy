package com.sda;

public class NumToArray {
    public static int[] convert(int input) {
        int number = input;
        int digits = 0;
        do {
            digits++;
            number /= 10;
        } while(number > 0);
        int [] array = new int[digits];

        int i = 0;
        number = input;
        int exponent = digits - 1;
        while (exponent >= 0) {
            array[i] = number / (int) Math.pow(10, exponent);
            number -= array[i] * (int) Math.pow(10, exponent);
            exponent--;
            i++;
        }
        return array;
    }

    public static int[] add(int[] tab1, int[] tab2) {
        if (tab1 == null) {
            return tab2;
        }
        if (tab2 == null) {
            return tab1;
        }
        int[] result = new int[tab1.length > tab2.length ? tab1.length : tab2.length];
        int tab1Idx = tab1.length - 1;
        int tab2Idx = tab2.length - 1;
        int mem = 0;
        for (int i = result.length - 1; i >= 0; i--) {
            int tmp = 0;
            if (tab1Idx >= 0 && tab2Idx >= 0) {
                tmp = tab1[tab1Idx] + tab2[tab2Idx];
            } else if (tab1Idx < 0) {
                tmp = tab2[tab2Idx];
            } else {
                tmp = tab1[tab1Idx];
            }
            if (mem > 0) {
                tmp += mem;
                mem = 0;
            }
            if (tmp > 9) {
                mem = 1;
                tmp = tmp % 10;
            }
            result[i] = tmp;
            tab1Idx--;
            tab2Idx--;
        }
        if (mem > 0) {
            int [] tmp = new int[result.length + 1];
            tmp[0] = mem;
            for (int i = 0; i < result.length; i++) {
                tmp[i+1] = result[i];
            }
            result = tmp;
        }
        return result;
    }
}
