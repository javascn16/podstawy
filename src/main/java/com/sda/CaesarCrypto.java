package com.sda;

public class CaesarCrypto {
    private int offset;

    public CaesarCrypto(int offset) {
        this.offset = offset;
    }

    public String cipher(String input) {
        String data = input.toLowerCase();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length(); i++) {
            char character = data.charAt(i);
            if (character >= 'a' && character <= 'z') {
                character += offset;
                if (character > 'z') {
                    character = (char)((character - 'z') + 'a');
                }
            }
            builder.append(character);
        }
        return builder.toString();
    }

    public String decipher(String input) {
        String data = input.toLowerCase();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length(); i++) {
            char character = data.charAt(i);
            if (character >= 'a' && character <= 'z') {
                character -= offset;
                if (character < 'a') {
                    character = (char)(character + 'z');
                }
            }
            builder.append(character);
        }
        return builder.toString();
    }
}
